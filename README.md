# Falabella Bravo API

## Sistema y libreria
*  Python 3.6
*  Django 2.1.5
*  Manejo de dependecias con PIP y requirements.txt
*  Postgresql
*  Redis y Celery para tareas asincronas

## Montar entorno de desarrollo

Pasos para montar entorno de desarrollo

*  Clonar repositorio
*  Instalar Docker y dentro de la carpeta correr: docker-compose up (o docker-compose up --build)
*  Configurar variables de entorno:
    *  DB_NAME
    *  DB_USER
    *  DB_HOST
    *  DB_PORT
    *  DB_PASSWORD
    *  AZURE_ACCOUNT_NAME
    *  AZURE_ACCOUNT_KEY
    *  AZURE_CONTAINER

## Estructura proyecto

*  backend/ Clase customizada para media storage con Azure
*  collaborators/  App con modelo base de usuarios y preferencias
*  falabella/ Proyecto django con las configuraciones generales
*  social/ App con modelo de datos de reconocimientos y interacciones sociales
*  stats/ App con modelo de datos de sesiones y estadisticas de uso dentro de la app
