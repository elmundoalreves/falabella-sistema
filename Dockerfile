FROM python:3.6-alpine
RUN apk update
RUN apk add build-base
RUN apk add jpeg-dev
RUN apk add zlib-dev
RUN apk add postgresql-dev
RUN apk add python-dev
RUN apk add libffi-dev
#RUN apk add openssl-dev
ENV LIBRARY_PATH=/lib:/usr/lib

ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
RUN pip install pillow

CMD ["python3"]
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

EXPOSE 8000