
from .models import *
# DJANGO
from django.contrib.auth.models import User
# DRF
from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField

class ProfileSerializer(serializers.ModelSerializer):

    area = serializers.StringRelatedField()
    avatar = Base64ImageField(required=False)

    thumbnail = HyperlinkedSorlImageField(
        '150x150',
        options={"quality": 80, "crop": "center"},
        source='avatar',
        read_only=True
    )

    class Meta:
        model = Profile
        exclude = ('id', 'user',)


class UserSerializer(serializers.ModelSerializer):

    profile = ProfileSerializer(read_only=True)
    
    class Meta:
        model = User
        fields = ('id','first_name', 'last_name', 'profile')


class UserProfileSerializer(UserSerializer):

    send = serializers.SerializerMethodField()
    received = serializers.SerializerMethodField()
    notifications = serializers.SerializerMethodField()

    def get_send(self, instance):
        return instance.posts.count()

    def get_received(self, instance):
        return instance.acknowledgments_received.count()

    def get_notifications(self, instance):
        return instance.notifications.filter(unread=True).count()

    class Meta:
        model = User
        fields = ('id','first_name', 'last_name', 'profile', 'send', 'received', 'notifications', 'is_staff')

class AreaSerializer(serializers.ModelSerializer):

    members = serializers.SerializerMethodField()

    def get_members(self, instance):
        return UserSerializer(User.objects.filter(profile__area=instance), many=True).data

    class Meta:
        model = Area
        exclude = ('id',)


class SubAreaSerializer(serializers.ModelSerializer):

    areas = AreaSerializer(many=True)

    class Meta:
        model = SubArea
        exclude = ('id',)        