# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings


class SubArea(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        verbose_name = u"Nivel de organizacion"
        verbose_name_plural = "Niveles organizacionales"



class Area(models.Model):
    name = models.CharField(max_length=255)
    subarea = models.ForeignKey(SubArea, on_delete=models.CASCADE, related_name="areas")

    class Meta:
        ordering = ('name',)
        
    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Area"
        verbose_name_plural = "Areas"

SEX_TYPE = (
    ("F", "Femenino"),
    ("M", u"Masculino"),
)

class Profile(models.Model):
    rut = models.CharField(max_length=255, default='1234567')
    dv = models.CharField(max_length=1, default='0')
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="profile")
    avatar = models.ImageField(upload_to="profiles", default="profiles/default.png")
    position = models.CharField(max_length=255, null=True, blank=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE, null=True, blank=True, related_name="profiles")
    gender = models.CharField("Sexo", max_length=1, choices=SEX_TYPE, null=True, blank=True)
    code_boss = models.CharField("RUT sin dv Jefe", max_length=255, null=True, blank=True)