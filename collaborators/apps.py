from django.apps import AppConfig


class CollaboratorsConfig(AppConfig):
    name = 'collaborators'
    verbose_name = 'Colaboradores'
