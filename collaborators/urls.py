from django.conf.urls import url, include
from .api import *


urlpatterns = [
    url(r'^profile/edit/$', EditProfileAPIView.as_view(), name="api_edit_profile"),
    url(r'^profile/$', ProfileUserAPIView.as_view(), name="api_profile"),
    url(r'^team/$', TeamAPIView.as_view(), name="api_team"),
    url(r'^users/$', UsersListAPIView.as_view(), name="api_users"),
    url(r'^users/(?P<id_user>\d+)/$', ProfileUserAPIView.as_view(), name="api_users"),
    url(r'^users/(?P<id_user>\d+)/recognized/$', UsersListRecognizedAPIView.as_view(), name="api_users_recognized"),
    url(r'^top-global/$', TopGlobalAPIView.as_view(), name="api_top_global"),
    url(r'^top-local/$', TopAreaAPIView.as_view(), name="api_top_local"),
    url(r'^falanet/$', FalanetAPIView.as_view(), name="api_falanet"),

    
]