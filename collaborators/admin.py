from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import *
from .tasks import *
from import_export.admin import ImportExportModelAdmin


@admin.register(Profile)
class ProfileAdmin(ImportExportModelAdmin):
    resource_class = ProfileResource
    

class ProfileInLine(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Perfil'

admin.site.unregister(User)
@admin.register(User)
class UserAdminNew(UserAdmin):
    inlines = (ProfileInLine, )
    list_filter = ('is_staff', 'last_login', )
    list_display = ('username', 'email', 'name', 'is_staff', 'is_active', 'last_login')

    def name(self, instance):
        return "%s %s" % (instance.first_name,instance.last_name)
    name.short_description = 'Nombre'


@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
    list_display = ('subarea', 'name',)

@admin.register(SubArea)
class SubAreaAdmin(admin.ModelAdmin):
    list_display = ('name',)

