# coding:utf-8
from __future__ import unicode_literals
import operator
from django.db import models
from django.db.models.constants import LOOKUP_SEP
from rest_framework.filters import SearchFilter

class UnaccentSearchFilter(SearchFilter):
    
    def construct_search(self, field_name):
        lookup = self.lookup_prefixes.get(field_name[0])
        if lookup:
            field_name = field_name[1:]
        else:
            lookup = 'unaccent__icontains'
        return LOOKUP_SEP.join([field_name, lookup])