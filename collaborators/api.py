# Django
from django.contrib.auth.models import User
from django.db.models import Count
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.shortcuts import render, redirect, get_object_or_404
# Apps
from .serializers import *
from stats.models import Session
from .filters import UnaccentSearchFilter
# Third Parties
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination

class StaffPermission(permissions.BasePermission):
    """
    Staff permission
    """
    def has_permission(self, request, view):
        return False if not request.user.is_staff else True

class AuthUserProfile:
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)


class StaffUserProfile:
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, StaffPermission)

class EditProfileAPIView(AuthUserProfile, generics.UpdateAPIView):
    """
    Editar Perfil
    """
    serializer_class = ProfileSerializer

    def get_object(self):
        return self.request.user.profile
    

class ProfileUserAPIView(AuthUserProfile, generics.RetrieveAPIView):
    """
    Perfil
    """
    serializer_class = UserProfileSerializer
    queryset = User.objects.all()

    def get_object(self):
        if 'id_user' in self.kwargs.keys():
            return get_object_or_404(User, pk=self.kwargs['id_user'])
        return self.request.user

    def finalize_response(self, request, response, *args, **kwargs):
        response = super(ProfileUserAPIView, self).finalize_response(request, response, *args, **kwargs)

        if 'id_user' not in self.kwargs.keys():
            try:
                session = Session.objects.get(user=self.request.user, date=timezone.now().date())
            except ObjectDoesNotExist:
                try:
                    session = Session.objects.get_or_create(user=self.request.user, date=timezone.now().date())
                except IntegrityError:
                    pass
            
        return response        




class TeamAPIView(AuthUserProfile, generics.RetrieveAPIView):
    """
    Equipo y areas
    """
    serializer_class = SubAreaSerializer

    def get_object(self):
        return self.request.user.profile.area.subarea



class UsersPagination(PageNumberPagination):
    page_size = 10

class UsersListAPIView(AuthUserProfile, generics.ListAPIView):
    """
    Buscador global de usuarios
    """
    serializer_class = UserSerializer
    pagination_class = UsersPagination
    filter_backends = (UnaccentSearchFilter, )
    search_fields = ('username', 'profile__position', 'profile__area__name', 'profile__area__subarea__name', 'email', 'first_name', 'last_name')
    queryset = User.objects.filter(is_staff=False)


class UsersListRecognizedAPIView(AuthUserProfile, generics.ListAPIView):
    """
    Usuarios que he reconocido
    """
    serializer_class = UserSerializer
    pagination_class = UsersPagination
    filter_backends = (UnaccentSearchFilter, )
    search_fields = ('username', 'profile__position', 'profile__area__name', 'profile__area__subarea__name', 'email', 'first_name', 'last_name')
    queryset = User.objects.filter(is_staff=False)

    def get_queryset(self):

        qs = self.queryset.filter(acknowledgments_received__post__user=self.kwargs['id_user']).distinct()

        return qs


class TopGlobalAPIView(AuthUserProfile, APIView):
    """
    Top Global
    """

    def get(self, request):
        recognized_qs = User.objects.annotate(acknowledgments=Count('acknowledgments_received'))\
        .filter(acknowledgments__gte=1)\
        .order_by('-acknowledgments')[:3]

        recognizers_qs = User.objects.annotate(acknowledgments=Count('posts'))\
        .filter(acknowledgments__gte=1)\
        .order_by('-acknowledgments')[:3]

        data = {
            'recognized': UserProfileSerializer(recognized_qs, context={'request': request}, many=True, read_only=True).data,
            'recognizers': UserProfileSerializer(recognizers_qs, context={'request': request}, many=True, read_only=True).data,
        }
        return Response(data)


class TopAreaAPIView(AuthUserProfile, APIView):
    """
    Top del area
    """
    def get(self, request):
        recognized_qs = User.objects.annotate(acknowledgments=Count('acknowledgments_received'))\
        .filter(acknowledgments__gte=1, profile__area__subarea=self.request.user.profile.area.subarea)\
        .order_by('-acknowledgments')[:3]

        recognizers_qs = User.objects.annotate(acknowledgments=Count('posts'))\
        .filter(acknowledgments__gte=1, profile__area__subarea=self.request.user.profile.area.subarea)\
        .order_by('-acknowledgments')[:3]

        data = {
            'recognized_qs': UserProfileSerializer(recognized_qs, context={'request': request}, many=True, read_only=True).data,
            'recognizers': UserProfileSerializer(recognizers_qs, context={'request': request}, many=True, read_only=True).data,
        }
        return Response(data)        



import Crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64

class FalanetAPIView(APIView):
    
    """
    Falanet integracion
    """
    def get(self, request):
        
        token = request.GET.get('token')
        privatekey = open("Llave/id_rsa.pem", "r").read()
        rsa_privatekey = RSA.importKey(privatekey)
        decryptor = PKCS1_OAEP.new(rsa_privatekey)
        
        try:
            token_decoded = base64.b64decode(token)
            decrypted = decryptor.decrypt(token_decoded)
            user_data = decrypted.decode("utf-8").split('|')

            '''
            rut = user_data[0]

            try:
                user = User.objects.get(username=rut)
            except Exception as e:
                user = User.objects.create_user(username=rut, password=rut[0:4])
            '''

            data = {
                'status': True,
                'decoed': user_data
            }
            return Response(data)  

        except Exception as e:
            return Response({'status':False})  