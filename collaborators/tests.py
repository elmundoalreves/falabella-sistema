# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

from .models import *
# Django
from django.urls import reverse
# DRF
from rest_framework.authtoken.models import Token

class TestLogged(APITestCase):

    def setUp(self):
        username = "test@test.com"
        self.user = User.objects.create(
            username=username,
            first_name="Test",
            password="qweqweqwe",
            email=username,)

        self.user2 = User.objects.create(
            username="test2@test.com",
            first_name="Test 2",
            password="qweqweqwe")

        
        token = Token.objects.create(user=self.user)
        profile = Profile.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

class TestAdminLogged(APITestCase):

    def setUp(self):
        username = "test@test.com"
        self.user = User.objects.create(
            username=username,
            first_name="Test",
            password="qweqweqwe",
            email=username,
            is_staff=True)

        self.user2 = User.objects.create(
            username="test2@test.com",
            first_name="Test 2",
            password="qweqweqwe")

        
        token = Token.objects.create(user=self.user)
        profile = Profile.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

class TestEditProfile(TestLogged):

    def test_edit_avatar(self):

        payload = {
            "avatar":"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAQ0lEQVR42u3PQREAAAgDINc/9Mzg14MGpNPOAxERERERERERERERERERERERERERERERERERERERERERERERERERuVivGpWdqNGJtQAAAABJRU5ErkJggg=="
        }

        url = reverse("api_edit_profile")
        response = self.client.patch(url, payload, format="json")
        print("Test editar")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_position(self):

        payload = {
            "position":"Jefazo"
        }

        url = reverse("api_edit_profile")
        response = self.client.patch(url, payload, format="json")
        print("Test editar")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)        