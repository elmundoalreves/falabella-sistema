# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
# Django core
from django.contrib.auth.models import User
# Apps
from .models import *
# Thirdy party
from celery.decorators import task
from celery import shared_task
from import_export import resources, fields, widgets


class ProfileResource(resources.ModelResource):
    subarea = fields.Field()
    area = fields.Field()
    nombre = fields.Field()
    materno = fields.Field()
    paterno = fields.Field()
    #rut = fields.Field()
    #dv = fields.Field()

    
    class Meta:
        model = Profile
        import_id_fields = ['rut']
        #exclude = ('fecha_inicio_sesion',)
        fields = ('id', 'subarea', 'area', 'nombre', 'materno', 'paterno', 'gender', 'position', 'rut', 'dv', 'code_boss' )
        #export_order = ('rut', 'cargo', 'unidad', 'nombre', 'apellidos', 'fecha_nacimiento', 'sexo', 'correo', 'codigo_jefe', 'anexo', 'zona')
        skip_unchanged = True
        report_skipped = True
    
    '''
    def save_instance(self, instance, using_transactions=True, dry_run=False):
        """
        Takes care of saving the object to the database.
        Keep in mind that this is done by calling ``instance.save()``, so
        objects are not created in bulk!
        """
        self.before_save_instance(instance, using_transactions, dry_run)
        if not using_transactions and dry_run:
            # we don't have transactions and we want to do a dry_run
            pass
        else:
            instance.save()
        self.after_save_instance(instance, using_transactions, dry_run)
    '''

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        if dataset.headers:
            dataset.headers = [str(header).lower().strip() for header in dataset.headers]
            #if you already have a id column in your file with values for this column then don't use below code
        if not dry_run and 'id' not in dataset.headers:
            dataset.headers.append('id')
   
    '''
    def before_import_row(self, row, **kwargs):

        row['rut'] = "{0}-{1}".format(row['rut'], row['dv'])
    '''

    def after_import_row(self, row, row_result, **kwargs):
        instance = self._meta.model.objects.get(pk=row_result.object_id)

        if 'subarea' in row.keys():
            subarea, created_s = SubArea.objects.get_or_create(name=row['subarea'].title())
            area, created_u = Area.objects.get_or_create(name=row['area'].title(), subarea=subarea)
            instance.area = area
        else:
            subarea, created_s = SubArea.objects.get_or_create(name='Sin especificar')
            area, created_u = Area.objects.get_or_create(name='Sin especificar', subarea=subarea)
            instance.area = area

        user = instance.user
        user.first_name = row['nombre'].title()
        user.last_name = u"{0} {1}".format(row['paterno'].title(), row['materno'].title())
        user.save()
        #instance.rut = "{0}-{1}".format(row['rut'].strip(), row['dv'].strip())
        
        instance.save()

    
    def before_save_instance(self, instance, using_transactions, dry_run , **kwargs):
        #print field.get_value(instance).all()
        rut = "{0}-{1}".format(instance.rut, instance.dv)
        print(instance.rut)
        try:
            user = User.objects.get(username=rut)
        except Exception as e:
            user = User.objects.create_user(username=rut, password=rut[0:4])
            
        instance.user = user


    