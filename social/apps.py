from django.apps import AppConfig


class SocialConfig(AppConfig):
    name = 'social'
    verbose_name = 'Reconocimientos Sociales'

    def ready(self):
        import social.signals