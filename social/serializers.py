import calendar
import datetime
# Django
from django.contrib.auth.models import User
from django.utils import timezone
# Apps
from .models import *
from collaborators.serializers import UserSerializer
# Third Parties
from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField

class BannerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Banner
        exclude = ('date_publish', 'date_unpublish')

class BannerAdminSerializer(BannerSerializer):
    image = Base64ImageField(required=True)
    class Meta:
        model = Banner
        fields = "__all__"

class ValueSerializer(serializers.ModelSerializer):

    received = serializers.SerializerMethodField()

    def get_serializer_context(self):
        return {"id_user": self.kwargs['id_user']}

    def get_received(self, instance):
        if 'id_user' in self.context['view'].kwargs.keys():
            return instance.acknowledgments.filter(user=self.context['view'].kwargs['id_user']).count()
        return instance.acknowledgments.filter(user=self.context["request"].user).count()

    class Meta:
        model = Value
        exclude = ('order',)

class ValueAdminSerializer(BannerSerializer):
    
    icon = Base64ImageField(required=True)

    class Meta:
        model = Value
        fields = "__all__"

class ValueField(serializers.RelatedField):
    def to_internal_value(self, data):
        return Value.objects.get(pk=int(data))

    def to_representation(self, value):
        return ValueSerializer(value, context=self.context).data

class UserField(serializers.RelatedField):
    def to_internal_value(self, data):
        return User.objects.get(pk=int(data))
        
    def to_representation(self, value):
        return UserSerializer(value, context=self.context).data

class RecognitionSerializer(serializers.ModelSerializer):
    value = ValueField(queryset=Value.objects.all())
    user = UserField(queryset=User.objects.all())

    class Meta:
        model = Recognition
        exclude = ('post',)



class PostSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    total_comments = serializers.SerializerMethodField()
    total_likes = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()
    date = serializers.DateTimeField(read_only=True)
    recognition = RecognitionSerializer()
    image = Base64ImageField(required=False)
    thumbnail = HyperlinkedSorlImageField(
        '600x450',
        options={"quality": 80, "crop": "center"},
        source='image',
        read_only=True
    )
    owner = serializers.SerializerMethodField()
    
    def get_owner(self, obj):
        return self.context["request"].user.pk == obj.user.pk

    def get_total_comments(self, instance):
        return instance.comments.count()

    def get_total_likes(self, instance):
        return instance.likes.count()

    def get_like(self, instance):
        return instance.likes.filter(user=self.context["request"].user).first() != None

    def create(self, validated_data):
        recognition_data = validated_data.pop('recognition')
        post = Post.objects.create(**validated_data)
        Recognition.objects.create(post=post, **recognition_data)
        return post

    class Meta:
        model = Post
        exclude = ('active',)




class ValueStoriesSerializer(serializers.ModelSerializer):

    stories = serializers.SerializerMethodField()
    total_recognized = serializers.SerializerMethodField()

    def get_stories(self, instance):

        today = timezone.now().date()
        month_range = calendar.monthrange(today.year,today.month)
        init_date_month = datetime.date(today.year, today.month, 1)
        last_date_month = datetime.date(today.year, today.month, month_range[1]) 
        # .exclude(image__exact='')\
        return PostSerializer(Post.objects.filter(date__range=[init_date_month, last_date_month], recognition__value=instance)\
        , many=True, context=self.context).data

    def get_total_recognized(self, instance):
        today = timezone.now().date()
        month_range = calendar.monthrange(today.year,today.month)
        init_date_month = datetime.date(today.year, today.month, 1)
        last_date_month = datetime.date(today.year, today.month, month_range[1]) 
        return Post.objects.filter(date__range=[init_date_month, last_date_month], recognition__value=instance).count()

    class Meta:
        model = Value
        exclude = ('order',)


class LikeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Like
        exclude = ('post', 'user',)

    def create(self, data):
        like, created = Like.objects.get_or_create(**data)
        return like

class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    owner = serializers.SerializerMethodField()
    post = serializers.PrimaryKeyRelatedField(read_only=True)

    def get_owner(self, obj):
        return self.context["request"].user.pk == obj.user.pk

    class Meta:
        model = Comment
        exclude = ('active', )

class ReportSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Report
        exclude = ('post',)


class NotificationSerializer(serializers.ModelSerializer):

    comment = CommentSerializer(read_only=True)
    post = PostSerializer(read_only=True)

    class Meta:
        model = Notification
        exclude = ('user',)    