# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from colorfield.fields import ColorField
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError



class Banner(models.Model):
    title = models.CharField("Titulo", max_length=200)
    image = models.ImageField("Banner", upload_to="banner", help_text="Tamaño recomendado de 750 × 1332 y en formato JPG")
    date_publish = models.DateField("Fecha de publicacion", null=True, blank=True, unique=True)
    date_unpublish = models.DateField("Fecha despublicar", null=True, blank=True, unique=True)

    class Meta:
        verbose_name = "Banner diario"
        verbose_name_plural = "Banners diarios"

    def __unicode__(self):
        return self.title
    def __str__(self):
        return self.title

class Value(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=255)
    icon = models.ImageField(upload_to="contenidos")
    color = ColorField(default='#F0F2F6')
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ("order",)
        verbose_name = "Valor"
        verbose_name_plural = "Valores"

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="posts")
    #recognition = models.ForeignKey(Recognition, related_name="posts", on_delete=models.CASCADE, verbose_name="Valor")
    content = models.TextField()
    image = models.ImageField(upload_to="video", null=True, blank=True, help_text="Tamaño recomendado de 1080px by 1920px (9:16) y en formato JPG")
    video = models.FileField("Video Mp4",upload_to='media', null=True, blank=True)
    active = models.BooleanField(default=True)
    date = models.DateTimeField(u"Fecha de publicación", default=timezone.now, editable=True)

    class Meta:
        ordering = ("-date",)
        verbose_name = "Post"
        verbose_name_plural = "Publicaciones"

    def __unicode__(self):
        return self.content

    def __str__(self):
        return self.content      

class Recognition(models.Model):
    post = models.OneToOneField(Post, on_delete=models.CASCADE, related_name="recognition")
    value = models.ForeignKey(Value, related_name="acknowledgments", on_delete=models.CASCADE, verbose_name="Valor")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="acknowledgments_received")
    date = models.DateTimeField(auto_now_add=True)


class Report(models.Model):
    post = models.OneToOneField(Post, on_delete=models.CASCADE, related_name="reports")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="reports")
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.comment

    def __str__(self):
        return self.comment    

class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="likes")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="likes")
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("post", "user")
        verbose_name = u"Reacción"
        verbose_name_plural = "Reacciones"

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="comments")
    comment = models.TextField()
    active = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-date",)
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios"

    def __unicode__(self):
        return self.comment

    def __str__(self):
        return self.comment           
        

class Notification(models.Model):
    post = models.ForeignKey(Post, verbose_name="Reconocimiento", on_delete=models.CASCADE, related_name="notifications", blank=True, null=True)
    comment = models.ForeignKey(Comment, verbose_name="Comentario", on_delete=models.CASCADE, related_name="notifications", blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="notifications")
    date = models.DateTimeField(auto_now_add=True)
    unread = models.BooleanField(default=True)

    class Meta:
        ordering = ("-date",)

    def clean(self):
        if self.post is None and self.comment is None:
            raise ValidationError(u'Debes seleccionar un reconocimiento o comentario nuevo para notificar')        