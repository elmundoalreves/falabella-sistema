# Generated by Django 2.2.1 on 2019-05-13 14:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0006_auto_20190513_1328'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Titulo')),
                ('image', models.ImageField(help_text='Tamaño recomendado de 750\u2006×\u20061332 y en formato JPG', upload_to='banner', verbose_name='Banner')),
                ('date_publish', models.DateField(blank=True, null=True, unique=True, verbose_name='Fecha de publicacion')),
                ('date_unpublish', models.DateField(blank=True, null=True, unique=True, verbose_name='Fecha despublicar')),
            ],
            options={
                'verbose_name': 'Banner diario',
                'verbose_name_plural': 'Banners diarios',
            },
        ),
        migrations.AlterField(
            model_name='notification',
            name='comment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='social.Comment', verbose_name='Comentario'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='post',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='social.Post', verbose_name='Reconocimiento'),
        ),
    ]
