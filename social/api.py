
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import calendar
import datetime
# Django core
from django.shortcuts import get_object_or_404
from django.db.models import Count
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import gettext as _
# App
from .serializers import *
from collaborators.api import AuthUserProfile, StaffUserProfile
from collaborators.serializers import UserSerializer
from collaborators.models import Area
from stats.models import Session
from collaborators.filters import UnaccentSearchFilter
# Third Parties
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework import exceptions
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import DateRangeFilter, DateFilter, ModelChoiceFilter
from django_filters import rest_framework as filters

class CustomizedPagination(PageNumberPagination):
    page_size = 25


class BannerAPIView(AuthUserProfile, generics.RetrieveAPIView):
    
    serializer_class = BannerSerializer

    def get_object(self):
        today = timezone.now().date()
        try:
            return Banner.objects.get(date_publish=today)
        except Banner.DoesNotExist:
            queryset = Banner.objects.filter(date_publish__lte=today, date_unpublish__gte=today).order_by('?').first()
            if queryset is not None:
                return queryset 
            else:
                raise exceptions.NotFound(detail=_("There is no banner for today"))


class ValueListAPIView(AuthUserProfile, generics.ListAPIView):
    """
    Valores
    """        
    serializer_class = ValueSerializer
    queryset = Value.objects.all()

class PostsAPIView(AuthUserProfile, generics.ListCreateAPIView): 
    """
    Reconocimientos de mi área
    """          
    serializer_class = PostSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        user = self.request.user
        return Post.objects.filter(user__profile__area__subarea=user.profile.area.subarea, active=True)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)


class PostDetailsApiView(AuthUserProfile, generics.RetrieveAPIView):
    """
    Ver un post de un reconocimiento
    """          
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_object(self):
        return get_object_or_404(Post, pk=self.kwargs["id_post"])


class PostEditApiView(AuthUserProfile, generics.DestroyAPIView, generics.UpdateAPIView):
    """
    Editar un post de un reconocimiento
    """          
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_object(self):
        return get_object_or_404(Post, pk=self.kwargs["id_post"], user=self.request.user)


class PostsUserListAPIView(AuthUserProfile, generics.ListAPIView):
    """
    Reconocimientos recibidos
    """    
    serializer_class = PostSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        if 'id_user' in self.kwargs.keys():
            return Post.objects.filter(recognition__user=self.kwargs['id_user'], active=True)
        user = self.request.user
        return Post.objects.filter(recognition__user=user, active=True)


class LikeAPIView(AuthUserProfile, generics.CreateAPIView, generics.DestroyAPIView):
    """
    Reaccionar a un reconocimiento
    """      
    serializer_class = LikeSerializer

    def perform_create(self, serializer):
        post = get_object_or_404(Post, pk=self.kwargs["id_post"])
        serializer.save(user=self.request.user, post=post)

    def get_object(self):
        return get_object_or_404(Like, post__pk=self.kwargs["id_post"], user=self.request.user)

class LikesUsersListAPIView(AuthUserProfile, generics.ListAPIView): 
    """
    Usuarios que reaccionaron a un reconocimiento
    """          
    serializer_class = UserSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        user_ids = Like.objects.filter(post__pk=self.kwargs["id_post"]).values_list("user", flat=True)
        return User.objects.filter(pk__in=user_ids)


class CommentCreateAPIView(AuthUserProfile, generics.CreateAPIView):
    """
    Comentar un reconocimiento
    """          
    serializer_class = CommentSerializer

    def perform_create(self, serializer):
        post = get_object_or_404(Post, pk=self.kwargs["id_post"])
        serializer.save(user=self.request.user, post=post)

class ReportCreateAPIView(AuthUserProfile, generics.CreateAPIView):
    """
    Reportar un reconocimiento
    """          
    serializer_class = ReportSerializer

    def perform_create(self, serializer):
        post = get_object_or_404(Post, pk=self.kwargs["id_post"])
        serializer.save(user=self.request.user, post=post)

class CommentEditAPIView(AuthUserProfile, generics.RetrieveUpdateDestroyAPIView):
    """
    Editar un comentario
    """          
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def get_object(self):
        return get_object_or_404(Comment, pk=self.kwargs["id_comment"], user=self.request.user)


class CommentsListAPIView(AuthUserProfile, generics.ListAPIView): 
    """
    Comentarios de un reconocimiento  
    """          
    serializer_class = CommentSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        return Comment.objects.filter(post__pk=self.kwargs["id_post"])

class StoriesAPIView(AuthUserProfile, generics.ListAPIView): 
    """
    Historias por Valor  
    """          
    serializer_class = ValueStoriesSerializer

    def get_queryset(self):

        today = timezone.now().date()
        month_range = calendar.monthrange(today.year,today.month)
        init_date_month = datetime.date(today.year, today.month, 1)
        last_date_month = datetime.date(today.year, today.month, month_range[1]) 

        return Value.objects.filter(acknowledgments__post__active=True, acknowledgments__date__range=[init_date_month, last_date_month])\
        .annotate(acknowledgments_total=Count('acknowledgments'))\
        .filter(acknowledgments_total__gte=1)\
        .order_by('-acknowledgments_total')


class NotificationListAPIView(AuthUserProfile, generics.ListAPIView):
    """
    Notificationes
    """    
    serializer_class = NotificationSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user)        


    def finalize_response(self, request, response, *args, **kwargs):
        # Marcamos las notificaciones como leidas una vez son consultadas via api
        notificaciones = Notification.objects.filter(user=self.request.user).exclude(unread=False)
        notificaciones.update(unread=False)
        return super(NotificationListAPIView, self).finalize_response(request, response, *args, **kwargs)


# API Administrador
class DashboardAPIView(StaffUserProfile, APIView):
    """
    Estadisticas para Dashboard
    """
    def get(self, request):
        today = timezone.now().date()
        date_week = today - timezone.timedelta(days=7)
        date_month = today - timezone.timedelta(days=30)
        data = {
            'acknowledgments_total': Post.objects.count(),
            'acknowledgments_week': Post.objects.filter(date__range=[date_week, today]).count(),
            'acknowledgments_month': Post.objects.filter(date__range=[date_month, today]).count(),
            'users_active': User.objects.exclude(last_login__isnull=True).count(),
            'users_active_week': Session.objects.filter(date__range=[date_week, today]).distinct('user').count(),
            'users_active_month': Session.objects.filter(date__range=[date_month, today]).distinct('user').count(),
        }
        return Response(data)

class PostFilter(filters.FilterSet):
    start_date = DateFilter(field_name='date',lookup_expr=('gt'),) 
    end_date = DateFilter(field_name='date',lookup_expr=('lt'))
    date_range = DateRangeFilter(field_name='date')
    user = ModelChoiceFilter(queryset=User.objects.all())
    value = ModelChoiceFilter(field_name='recognition__value',queryset=Value.objects.all())
    area = ModelChoiceFilter(field_name='user__profile__area', queryset=Area.objects.all())

    class Meta:
        model = Post
        fields = ('date', 'start_date','end_date', 'date_range', 'value', 'user')


class PostsAdminListAPIView(StaffUserProfile, generics.ListAPIView):
    """
    Administración de Reconocimientos
    """    
    serializer_class = PostSerializer
    pagination_class = CustomizedPagination
    filterset_class = PostFilter
    queryset = Post.objects.all()

    #filter_backends = (UnaccentSearchFilter, )
    #search_fields = ('content', 'user__first_name', 'user__last_name', 'user__email',)

class BannerAdminListAPIView(StaffUserProfile, generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    """
    Administracion de Banners
    """    
    serializer_class = BannerAdminSerializer
    pagination_class = CustomizedPagination

    def get_queryset(self):
        return Banner.objects.all()

class BannerEditAdminListAPIView(StaffUserProfile, generics.RetrieveUpdateDestroyAPIView):
    """
    Administracion edicion Banners
    """    
    serializer_class = BannerAdminSerializer
    queryset = Banner.objects.all()

class ValuesAdminListAPIView(StaffUserProfile, generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    """
    Administracion de Valores
    """    
    serializer_class = ValueAdminSerializer

    def get_queryset(self):
        return Value.objects.all()

class ValuesEditAdminListAPIView(StaffUserProfile, generics.RetrieveUpdateDestroyAPIView):
    """
    Administracion edicion Valores
    """    
    serializer_class = ValueAdminSerializer
    queryset = Value.objects.all()
