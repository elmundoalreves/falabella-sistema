from django.conf.urls import url, include
from .api import *


urlpatterns = [
    url(r'^banner/$', BannerAPIView.as_view(), name="api_banner"),
    url(r'^values/$', ValueListAPIView.as_view(), name="api_values"),
    url(r'^stories/$', StoriesAPIView.as_view(), name="api_stories"),
    url(r'^posts/$', PostsAPIView.as_view(), name="api_posts"),
    url(r'^acknowledgments/$', PostsUserListAPIView.as_view(), name="api_acknowledgments"),
    url(r'^posts/(?P<id_post>\d+)/$', PostDetailsApiView.as_view(), name="api_post"),
    url(r'^posts/(?P<id_post>\d+)/edit$', PostEditApiView.as_view(), name="api_post_edit"),
    url(r'^posts/(?P<id_post>\d+)/report$', ReportCreateAPIView.as_view(), name="api_report_post"),
    url(r'^posts/(?P<id_post>\d+)/like$', LikeAPIView.as_view(), name="api_like_post"),
    url(r'^posts/(?P<id_post>\d+)/likes$', LikesUsersListAPIView.as_view(), name="api_like_post_users"),
    url(r'^posts/(?P<id_post>\d+)/comment$', CommentCreateAPIView.as_view(), name="api_comment_post"),
    url(r'^comment/(?P<id_comment>\d+)/$', CommentEditAPIView.as_view(), name="api_comment_edit"),        
    url(r'^posts/(?P<id_post>\d+)/comments$', CommentsListAPIView.as_view(), name="api_comment_post_users"),
    url(r'^notifications/$', NotificationListAPIView.as_view(), name="api_notifications"),
    url(r'^user/(?P<id_user>\d+)/values/$', ValueListAPIView.as_view(), name="api_values_user"),
    url(r'^user/(?P<id_user>\d+)/acknowledgments/$', PostsUserListAPIView.as_view(), name="api_acknowledgments_user"),
    # ADMIN Endpoints
    url(r'^admin/dashboard/$', DashboardAPIView.as_view(), name="api_admin_posts"),
    url(r'^admin/posts/$', PostsAdminListAPIView.as_view(), name="api_admin_posts"),
    url(r'^admin/banners/$', BannerAdminListAPIView.as_view(), name="api_admin_banner"),
    url(r'^admin/banners/(?P<pk>\d+)/$', BannerEditAdminListAPIView.as_view(), name="api_admin_edit_banner"),
    url(r'^admin/values/$', ValuesAdminListAPIView.as_view(), name="api_admin_values"),
    url(r'^admin/values/(?P<pk>\d+)/$', ValuesEditAdminListAPIView.as_view(), name="api_admin_edit_values"),

]