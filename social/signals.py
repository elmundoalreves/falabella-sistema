from .models import *
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Recognition, dispatch_uid="save_recognition")
def save_recognition(sender, instance, created, **kwargs):
    if created:
        notification, created = Notification.objects.get_or_create(post=instance.post,user=instance.user)
        print("Notificacion", notification, created)


@receiver(post_save, sender=Comment, dispatch_uid="save_comment")
def save_comment(sender, instance, created, **kwargs):
    if created:
        # Notificamos solo si el comentario no es del dueño
        if instance.post.user.pk != instance.user.pk:
            notification, created = Notification.objects.get_or_create(comment=instance,user=instance.post.user)
            print("Notificacion", notification, created)        

@receiver(post_save, sender=Report, dispatch_uid="save_recognition")
def save_recognition(sender, instance, created, **kwargs):
    if created:
        instance.post.active = False        
        instance.post.save()