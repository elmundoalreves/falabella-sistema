from django.contrib import admin
from .models import *

@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_publish', 'date_unpublish')

@admin.register(Value)
class ValueAdmin(admin.ModelAdmin):
    list_display = ('title', 'color')


class RecognitionInLine(admin.StackedInline):
    model = Recognition
    can_delete = False
    verbose_name_plural = 'Reconocimiento'

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    inlines = (RecognitionInLine, )
    list_display = ('user', 'content', 'date', 'active')


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    pass

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('user', 'date', 'unread')

