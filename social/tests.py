# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from rest_framework import status
from collaborators.tests import TestLogged, TestAdminLogged
from django.urls import reverse
from .models import *
from django.contrib.auth.models import User

class TestPostRecognition(TestLogged):
    fixtures = ('fixture_social.json',)

    def test_create_social(self):
        user = User.objects.last()
        payload = {
            "content": "Buen trabajo en equipo",
            "image":"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAQ0lEQVR42u3PQREAAAgDINc/9Mzg14MGpNPOAxERERERERERERERERERERERERERERERERERERERERERERERERERuVivGpWdqNGJtQAAAABJRU5ErkJggg==",
            "recognition": {
                "value":2,
                "user": user.pk
            }
        }

        url = reverse("api_posts")
        response = self.client.post(url, payload, format="json")
        print("Test publicar")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Post.objects.count(), 1)
        self.assertEqual(Recognition.objects.count(), 1)

    def test_edit_post(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        
        payload = {
            "content": "Bien hecho!"
        }

        url = reverse("api_post_edit", kwargs={'id_post': post.id})
        response = self.client.patch(url, payload, format="json")
        print("Test editar")
        print(response.data)
        self.assertEqual(Post.objects.first().content, payload['content'])

    def test_delete_post(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        
        url = reverse("api_post_edit", kwargs={'id_post': post.id})
        response = self.client.delete(url)
        print("Test eliminar post")
        self.assertEqual(Post.objects.count(), 0)

    def test_comment_post(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        payload = {
            "comment": "Merecido!"
        }
        url = reverse("api_comment_post", kwargs={'id_post': post.id})
        response = self.client.post(url, payload, format="json")
        print("Test comentar")
        print(response.data)        
        self.assertEqual(Comment.objects.count(), 1)

    def test_comment_edit(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        comment = Comment.objects.create(
            post=post,
            user=user_1,
            comment='Comentario !'
        )

        payload = {
            "comment": "Merecido!"
        }
        url = reverse("api_comment_edit", kwargs={'id_comment': comment.id})
        response = self.client.patch(url, payload, format="json")
        print("Test editar comentario")
        print(response.data)        
        self.assertEqual(Comment.objects.first().comment, payload['comment'])


    def test_comment_delete(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        comment = Comment.objects.create(
            post=post,
            user=user_1,
            comment='Comentario !'
        )

        url = reverse("api_comment_edit", kwargs={'id_comment': comment.id})
        response = self.client.delete(url)
        print("Test eliminar comentario")
        self.assertEqual(Comment.objects.count(), 0)

    def test_report_post(self):
        user_1 = User.objects.first()
        user_2 = User.objects.last()
        value = Value.objects.first()
        post = Post.objects.create(
            user=user_1,
            content= "Buen trabajo en equipo"
        )
        recognition = Recognition.objects.create(
            post=post,
            value=value,
            user=user_2
        )
        payload = {
            "comment": "Mala vola!"
        }
        url = reverse("api_report_post", kwargs={'id_post': post.id})
        response = self.client.post(url, payload, format="json")
        print("Test reportar")
        print(response.data)       
        self.assertEqual(Report.objects.count(), 1) 
        self.assertEqual(Post.objects.filter(active=True).count(), 0)




class TestAdmin(TestAdminLogged):
    fixtures = ('fixture_social.json',)

    def test_created_value(self):

        payload = {
            "title": "Buen trabajo en equipo",
            "description": "Valor desde admin",
            "icon":"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAQ0lEQVR42u3PQREAAAgDINc/9Mzg14MGpNPOAxERERERERERERERERERERERERERERERERERERERERERERERERERuVivGpWdqNGJtQAAAABJRU5ErkJggg=="
        }

        url = reverse("api_admin_values")
        response = self.client.post(url, payload, format="json")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Value.objects.count(), 3)

    def test_edit_value(self):
        payload = {
            "title": "Valor editado",
            "icon":"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAQ0lEQVR42u3PQREAAAgDINc/9Mzg14MGpNPOAxERERERERERERERERERERERERERERERERERERERERERERERERERuVivGpWdqNGJtQAAAABJRU5ErkJggg=="
        }

        url = reverse("api_admin_edit_values", kwargs={'pk': Value.objects.first().pk})
        response = self.client.patch(url, payload, format="json")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_created_banner(self):

        payload = {
            "title": "Banner",
            "image":"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAQ0lEQVR42u3PQREAAAgDINc/9Mzg14MGpNPOAxERERERERERERERERERERERERERERERERERERERERERERERERERuVivGpWdqNGJtQAAAABJRU5ErkJggg==",
            "date_publish":"2019-05-13",
            "date_unpublish":"2019-05-21"
        }

        url = reverse("api_admin_banner")
        response = self.client.post(url, payload, format="json")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Banner.objects.count(), 2)        


    def test_edit_banner(self):
        payload = {
            "title": "Banner editado",
            "date_publish":"2019-05-01"
        }

        url = reverse("api_admin_edit_banner", kwargs={'pk': Banner.objects.first().pk})
        response = self.client.patch(url, payload, format="json")
        print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)        