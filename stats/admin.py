from django.contrib import admin
from .models import *

@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ('user', 'date')
