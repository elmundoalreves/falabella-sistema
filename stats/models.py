# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# Django
from django.db import models
from django.conf import settings

class Session(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, related_name="sesiones")
    date = models.DateField(auto_now_add=True)
    
    def __unicode__(self):
        return self.user.username

    class Meta:
        unique_together = (("user", "date"),)
        verbose_name = "Usuario diario"
        verbose_name_plural = "Usuarios diarios"